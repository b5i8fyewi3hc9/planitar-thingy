#Running the entire solution

`cd` into the directory containing the `docker-compose.yml` file

Run `docker-compose up --build` to build and run everything. 

The frontend alone can be accessed via localhost:8080
The backend alone can be accessed via localhost:9090

The entire app can be accessed from localhost

At the moment, both Dockerfiles are for 'dev' mode, 
meaning they don't copy the entire src code into the container,
but instead mount the src inside the container.

