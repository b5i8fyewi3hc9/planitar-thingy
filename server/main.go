package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

// High-tech in memory DB
var articles = make(map[string]string)

func createArticle(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		println(err)
		panic(err)
	}

	name := strings.TrimPrefix(r.URL.Path, "/articles/")

	// As per the assignment instructions, the following request should work:
	// curl -X PUT http://localhost:9090/articles/wiki -d 'A wiki is a knowledge base website'
	//
	// By default, the 'Content-Type' header for the above command is 'application/x-www-form-urlencoded',
	// so the text 'A wiki is a knowledge base website' is only accessible as a form key,
	// who's content is an empty string. That is why we iterate through the 'PostForm' keys
	// and use the first (and theoretically only) one as the article's content
	var content string
	for key, _ := range r.PostForm {
		content = key
		break
	}

	status := http.StatusOK
	if _, ok := articles[name]; !ok {
		status = http.StatusCreated
	}
	w.WriteHeader(status)

	articles[name] = content
}

func getArticles(w http.ResponseWriter, r *http.Request) {
	articleNames := make([]string, 0, len(articles))
	for name, _ := range articles {
		articleNames = append(articleNames, name)
	}

	articlesJson, err := json.Marshal(articleNames)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(articlesJson)
}

func getArticle(w http.ResponseWriter, r *http.Request) {
	name := strings.TrimPrefix(r.URL.Path, "/articles/")
	if articleContent, ok := articles[name]; !ok {
		w.WriteHeader(http.StatusNotFound)
	} else {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(articleContent))
	}
}

func articleHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		if r.URL.Path == "/articles/" {
			getArticles(w, r)
		} else {
			getArticle(w, r)
		}
		break
	case http.MethodPut:
		createArticle(w, r)
		break
	default:
		http.Error(w, "", http.StatusMethodNotAllowed)
	}
}

func main() {
	http.HandleFunc("/articles/", articleHandler)

	log.Fatal(http.ListenAndServe(":9090", nil))
}
