FROM golang:1.17.3-alpine3.14

WORKDIR /app/server

COPY go.mod ./
#COPY go.sum ./

RUN go mod download

#COPY *.go ./

EXPOSE 9090

#RUN go run

CMD [ "go", "run", "main.go" ]