FROM golang:1.17.3-alpine3.14

WORKDIR /app/server

COPY go.mod ./
#COPY go.sum ./

RUN go mod download

COPY *.go ./

RUN go build -o /planitar-server

EXPOSE 9090

CMD [ "/planitar-server" ]