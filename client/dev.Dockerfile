FROM node:lts-alpine

WORKDIR /app/client

COPY package*.json ./
COPY yarn.lock ./

RUN yarn

EXPOSE 8080

CMD ["npm", "run", "serve"]