const baseUrl = 'http://localhost/api/';
const articlesEndpoint = 'articles/';

const getArticleNames = async () => {
  try {
    const r = await fetch(`${baseUrl}${articlesEndpoint}`);
    const articles = await r.json();
    return articles;
  } catch (e) {
    console.error(e);
    return null;
  }
};

const getArticleContent = async (name) => {
  try {
    const r = await fetch(`${baseUrl}${articlesEndpoint}${name}`);
    const articleContent = await r.text();
    return articleContent;
  } catch (e) {
    console.error(e);
    return null;
  }
};

const saveArticle = async ({ name, content }) => {
  try {
    const r = await fetch(`${baseUrl}${articlesEndpoint}${name}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow',
      body: content,
    });
    return r;
  } catch (e) {
    console.error(e);
    return null;
  }
};

module.exports = {
  getArticleNames,
  getArticleContent,
  saveArticle,
};
