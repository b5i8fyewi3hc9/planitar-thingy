import Vue from 'vue';
import { getArticleContent, saveArticle } from '../../api/articles';

const { getArticleNames } = require('../../api/articles');

export default {
  namespaced: true,
  state: {
    selectedArticleName: '',
    articles: {},
  },
  mutations: {
    SET_ACTIVE_ARTICLE_NAME: (state, name) => {
      state.selectedArticleName = name;
    },

    SET_ARTICLE_NAMES: (state, names) => {
      names.forEach((name) => {
        if (!state.articles[name]) {
          Vue.set(state.articles, name, '');
        }
      });
    },

    SET_ARTICLE_CONTENT: (state, { name, content }) => {
      Vue.set(state.articles, name, content);
    },
  },
  actions: {
    getArticleNames: async ({ commit }) => {
      const names = await getArticleNames();
      commit('SET_ARTICLE_NAMES', names);
      return names;
    },

    getArticleContent: async ({ commit }, name) => {
      const content = await getArticleContent(name);
      if (content) {
        commit('SET_ARTICLE_CONTENT', { name, content });
      }
      return content;
    },

    saveArticle: async ({ commit }, { name, content }) => {
      const r = await saveArticle({ name, content });
      commit('SET_ARTICLE_CONTENT', { name, content });
      return r;
    },
  },
  getters: {
    articleList: (state) => state.articles,

    article: (state) => (name) => state.articles[name],
  },
  modules: {
  },
};
